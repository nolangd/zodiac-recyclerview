package edu.udmercy.basiczodiac

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private val listOfSigns = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.adapter = CustomAdapter(listOfSigns)

        prepareItems()
    }

    private fun prepareItems() {
        listOfSigns.add("aries")
        listOfSigns.add("taurus")
        listOfSigns.add("gemini")
        listOfSigns.add("cancer")
        listOfSigns.add("leo")
        listOfSigns.add("virgo")
        listOfSigns.add("libra")
        listOfSigns.add("scorpio")
        listOfSigns.add("sagittarius")
        listOfSigns.add("capricorn")
        listOfSigns.add("aquarius")
        listOfSigns.add("pisces")
    }
}